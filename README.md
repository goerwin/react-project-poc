# Run in development mode

```
$ npm run dev
```

# Build production assets

```
$ npm run build
```

# Tests

```
$ npm run test
```
