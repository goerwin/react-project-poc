import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
import Home from './Home/Home';
import Shows from './Shows/Shows';

render(
  <Router>
    <>
      <ul>
        <li><Link to="/">Home</Link></li>
        <li><Link to="/shows">Shows</Link></li>
      </ul>
      <Route exact path="/" component={Home} />
      <Route path="/shows" component={Shows} />
    </>
  </Router>
  , document.getElementById('app'));
