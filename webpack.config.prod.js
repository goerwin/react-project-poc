const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const getAbsPath = pathStr => path.resolve(__dirname, pathStr);

module.exports = {
  mode: 'production',
  entry: {
    index: getAbsPath('src/index.tsx')
  },
  output: {
    path: getAbsPath('dist'),
    filename: '[name].bundle.[chunkhash].js',
    publicPath: './'
  },
  devtool: 'source-map',
  module: {
    rules: [
      // Javascript/JSX
      {
        test: /\.jsx?$/,
        include: getAbsPath('src'),
        use: ['ts-loader']
      },

      // TypeScript/TSX
      {
        test: /\.tsx?$/,
        include: getAbsPath('src'),
        use: ['ts-loader']
      },

      // Scss
      {
        test: /\.s?css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader'
        ]
      },

      // Images
      {
        test: /\.(png|jpg|svg)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[hash].[ext]'
          }
        }
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: getAbsPath('src/index.html')
    }),

    new MiniCssExtractPlugin({
      name: '[name].[contenthash].css'
    }),

    new BundleAnalyzerPlugin({
      openAnalyzer: false,
      analyzerMode: 'static',
      reportFilename: 'webpack-bundle-analyzer.html'
    })
  ],

  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],

    // Where to look when using relative paths in imports. Eg. "import 'lodash'"
    modules: [getAbsPath('src'), getAbsPath('node_modules')]
  }
};
