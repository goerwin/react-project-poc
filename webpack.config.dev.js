const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const devServerPort = process.env.SERVER_PORT || 8000;
const analyzerPort = process.env.ANALYZER_PORT || 8888;
const generateStaticAnalyzer = !!process.env.ANALYZER;

const getAbsPath = pathStr => path.resolve(__dirname, pathStr);

module.exports = {
  mode: 'development',
  entry: {
    index: getAbsPath('src/index.tsx')
  },
  output: {
    path: getAbsPath('dist'),
    filename: '[name].bundle.js',
    publicPath: './'
  },
  devtool: 'cheap-eval-source-map',
  devServer: {
    publicPath: '/', // Necessary for index.html to work
    inline: false, // Don't inject livereload code
    host: '127.0.0.1', // Needed too
    port: devServerPort,
    historyApiFallback: true
  },

  module: {
    rules: [
      // Javascript/JSX
      {
        test: /\.jsx?$/,
        include: getAbsPath('src'),
        use: ['ts-loader']
      },

      // TypeScript/TSX
      {
        test: /\.tsx?$/,
        include: getAbsPath('src'),
        use: ['ts-loader']
      },

      // Scss
      {
        test: /\.s?css$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      },

      // Images
      {
        test: /\.(png|jpg|svg)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]'
          }
        }
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: getAbsPath('src/index.html')
    }),

    new BundleAnalyzerPlugin({
      openAnalyzer: false,
      ...generateStaticAnalyzer ?
        {
          analyzerMode: 'static',
          reportFilename: 'webpack-bundle-analyzer.html'
        }
        :
        {
          analyzerMode: 'server',
          analyzerPort
        }
    })
  ],

  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],

    // Where to look when using relative paths in imports. Eg. "import 'lodash'"
    modules: [getAbsPath('src'), getAbsPath('node_modules')]
  }
};
